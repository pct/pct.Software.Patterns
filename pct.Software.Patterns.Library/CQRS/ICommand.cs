﻿//
//  Copyright 2014  Michael T. Muessemeyer
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;
using pct.Software.Patterns.PubSub;

namespace pct.Software.Patterns.CQRS
{
    /// <summary>
    /// Provides a mechanism for notifying subscribers that handle commands.
    /// </summary>
    public interface ICommand: INotify
    {
        /// <summary>
        /// Gets the identifier of the aggregate for which the command is intended.
        /// </summary>
        /// <value>The aggregate identifier.</value>
        Guid AggregateId { get; }
    }
}
