﻿//
//  Copyright 2014  Michael T. Muessemeyer
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;

namespace pct.Software.Patterns.DataAccess
{
    /// <summary>
    /// Provides a mechanism to connect to a data store.
    /// </summary>
    public interface IDataStoreConnection<T> where T: IDataContext
    {
        /// <summary>
        /// Gets the string used to open the connection.
        /// </summary>
        /// <remarks>The exact contents of the connection string depend on the specific data store. See the documentation for the specific data store you are using.</remarks>
        /// <value>The connection string.</value>
        string ConnectionString { get; }

        /// <summary>
        /// Opens a connection to the underyling data store with the settings sepcified by the <see cref="ConnectionString"/>.
        /// </summary>
        T Connect();
    }
}

