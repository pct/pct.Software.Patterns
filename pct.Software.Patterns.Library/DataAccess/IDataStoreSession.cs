﻿//
//  Copyright 2014  Michael T. Muessemeyer
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;

namespace pct.Software.Patterns.DataAccess
{
    /// <summary>
    /// Provides a mechanism for abstracting a persistence store.
    /// </summary>
    public interface IDataStoreSession<T>: IDisposable
    {
        /// <summary>
        /// Opens the connection to an underlying persistence store.
        /// </summary>
        /// <returns>A <see cref="IUnitOfWork&lt;T&gt;"/> to track and save changes within this session.</returns>
        IUnitOfWork<T> Start();

        /// <summary>
        /// Ensures the connection to the underlying persistence store is disposed.
        /// </summary>
        void Finish();
    }
}

