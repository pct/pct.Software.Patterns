﻿//
//  Copyright 2014  Michael T. Muessemeyer
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;

namespace pct.Software.Patterns.DataAccess
{
    /// <summary>
    /// Provides a mechanism to access scoped data in a decoupled manner.
    /// </summary>
    /// <remarks>The common implementation of this interface is to expose data objects via properties of type <see cref="IReadOnlyRepository&lt;T&gt;"/>.</remarks>
    public interface IDataContext: IDisposable
    {
        /// <summary>
        /// Saves all changes made in this context to the underlying data store.
        /// </summary>
        /// <returns>The number of objects written to the underlying data store.</returns>
        int SaveChanges();
    }
}

