﻿//
//  Copyright 2014  Michael T. Muessemeyer
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;

namespace pct.Software.Patterns.DataAccess
{
    /// <summary>
    /// Defines a mechanism for mediating between the domain and data mapping layers using a collection-like interface for accessing entities.
    /// </summary>
    /// <typeparam name="T">The entity type that is accessible via this repository.</typeparam>
    public interface IRepository<T>: IReadOnlyRepository<T>
    {
        /// <summary>
        /// Adds the specified entity to the collection.
        /// </summary>
        /// <param name="anEntity">The entity that is to be added to the underlying data store.</param>
        void Add(T anEntity);

        /// <summary>
        /// Removes the specified entity from the collection.
        /// </summary>
        /// <param name="anEntity">The entity that is to be removed from the underlying data store.</param>
        void Remove(T anEntity);
    }
}
