﻿//
//  Copyright 2014  Michael T. Muessemeyer
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;

namespace pct.Software.Patterns.DataAccess
{
    /// <summary>
    /// Provides a mechanism to keep track of operations that occur during a given context.
    /// </summary>
    public interface IUnitOfWork<T>: IDisposable
    {
        /// <summary>
        /// Saves registered changes to the given context.
        /// </summary>
        void Complete();

        /// <summary>
        /// Registers the specified entity to be created in a given context upon completion of the unit of work.
        /// </summary>
        /// <param name="anEntity">An entity.</param>
        void RegisterCreated(T anEntity);

        /// <summary>
        /// Registers the specified entity to be updated in a given context upon completion of the unit of work.
        /// </summary>
        /// <param name="anEntity">An entity.</param>
        void RegisterModified(T anEntity);

        /// <summary>
        /// Registers the specified entity to be removed from a given context upon completion of the unit of work.
        /// </summary>
        /// <param name="anEntity">An entity.</param>
        void RegisterRemoved(T anEntity);
    }
}

