﻿//
//  Copyright 2014  Michael T. Muessemeyer
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;
using pct.Software.Patterns.PubSub;

namespace pct.Software.Patterns.EventSourcing
{
    /// <summary>
    /// A value type that represents the data structure of a <see cref="DDD.DomainEventBase"/> as 
    /// it is stored in an Event Store.
    /// </summary>
    public class EventRecord
    {
        /// <summary>
        /// Initializes a new instance of the this class.
        /// </summary>
        /// <param name="anAggregateId">The unique identifier of the referenced domain aggregate.</param>
        /// <param name="anEvent">The event that is to be recorded.</param>
        /// <param name="theEventType">The event's type.</param>
        /// <param name="expectedVersionOfAggregate">
        /// Expected version of the domain aggregate identified by anAggregateId.
        /// </param>
        /// <remarks>
        /// When using the Event Sourcing pattern along with Domain-Driven Design, the parameter
        /// <paramref name="anEvent"/> should be of type <see cref="pct.Software.Patterns.DDD.DomainEventBase"/> which 
        /// implements the <see cref="INotify"/> interface.
        /// </remarks>
        public EventRecord(Guid anAggregateId, INotify anEvent, string theEventType, int expectedVersionOfAggregate)
        {
            AggregateId = anAggregateId;
            DomainEvent = anEvent;
            EventType = theEventType;
            Version = expectedVersionOfAggregate;
        }

        /// <summary>
        /// Initializes a new instance of this class with no values.
        /// </summary>
        public EventRecord()
        {
        }

        /// <summary>
        /// Gets or sets the primary key value of the referenced record in the event store.
        /// </summary>
        /// <value>The unique identifier of this event record.</value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the aggregate identifier.
        /// </summary>
        /// <value>The aggregate identifier.</value>
        public Guid AggregateId { get; set; }

        /// <summary>
        /// Gets or sets the domain event being recorded.
        /// </summary>
        /// <value>The recorded domain event.</value>
        public INotify DomainEvent { get; set; }

        /// <summary>
        /// Gets or sets the domain event type.
        /// <value>The type of the recorded domain event.</value>
        /// </summary>
        public string EventType { get; set; }

        /// <summary>
        /// Gets or sets the version of the aggregate identified by AggregateId.
        /// </summary>
        /// <value>The version number.</value>
        public int Version { get; set; }
    }
}