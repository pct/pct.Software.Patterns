﻿//
//  Copyright 2014  Michael T. Muessemeyer
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;

namespace pct.Software.Patterns.EventSourcing
{
    /// <summary>
    /// A value type that represents the data structure of an <see cref="DDD.IAggregate"/> as it is stored in an Event Store.
    /// </summary>
    public class AggregateRecord
    {
        /// <summary>
        /// Initializes a new instance of this class.
        /// </summary>
        /// <param name="anAggregateId">The unique identifier of the referenced domain aggregate.</param>
        /// <param name="typeOfAggregate">String representing the type of the domain aggregate.</param>
        /// <param name="version">Current version of the domain aggregate, used to avoid concurrency issues.</param>
        public AggregateRecord(Guid anAggregateId, string typeOfAggregate, int version)
        {
            AggregateId = anAggregateId;
            AggregateType = typeOfAggregate;
            Version = version;
        }

        /// <summary>
        /// Constructs a new instance of this class.
        /// </summary>
        public AggregateRecord()
        {
        }

        /// <summary>
        /// Gets or sets the primary key value of the referenced record in the event store.
        /// </summary>
        /// <value>The unique identifier of this aggregate record.</value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the aggregate identifier.
        /// </summary>
        /// <value>The aggregate identifier.</value>
        public Guid AggregateId { get; set; }

        /// <summary>
        /// Gets or sets the type of the aggregate.
        /// </summary>
        /// <value>The type of the aggregate.</value>
        public string AggregateType { get; set; }

        /// <summary>
        /// Gets or sets the version of the domain aggregate identified by the AggregateId, used to avoid concurrency issues.
        /// </summary>
        /// <value>The version.</value>
        public int Version { get; set; }
    }
}
