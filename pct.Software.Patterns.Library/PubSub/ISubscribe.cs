﻿//
//  Copyright 2014  Michael T. Muessemeyer
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;

namespace pct.Software.Patterns.PubSub
{
    /// <summary>
    /// Provides a mechanism for representing a subscriber of notification messages in a Pub/Sub collaboration.
    /// </summary>
    public interface ISubscribe
    {
        /// <summary>
        /// Deliver the specified <see cref="INotify"/> message.
        /// </summary>
        /// <param name="aMessage">A message.</param>
        void Deliver(INotify aMessage);
    }
}

