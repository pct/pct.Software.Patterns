﻿//
//  Copyright 2014  Michael T. Muessemeyer
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;

namespace pct.Software.Patterns.PubSub
{
    /// <summary>
    /// Provides a mechanism for notifying a subscriber in a Publish/Subscribe pattern collaboration.
    /// </summary>
    public interface INotify
    {
        /// <summary>
        /// Notify the specified <see cref="ISubscribe"/> using a Visitor pattern for delivering the message.
        /// </summary>
        /// <param name="aSubscriber">A subscriber.</param>
        void Notify(ISubscribe aSubscriber);
    }
}

