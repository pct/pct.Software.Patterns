﻿//
//  Copyright 2014  Michael T. Muessemeyer
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;

namespace pct.Software.Patterns.DDD
{
    /// <summary>
    /// A base class for implementing a data structure intended to be treated a value object in a Domain-Driven Design collaboration.
    /// </summary>
    /// <remarks>
    /// <para>When you care only about the attributes and logic of an element of the model, classify it as a value 
    /// object. Make it express the meaning of the attributes it conveys and give it related functionality. Treat the 
    /// value object as immutable. Make all operations Side-effect-free Functions that don’t depend on any mutable 
    /// state. Don’t give a value object any identity and avoid the design complexities necessary to 
    /// maintain entities. </para>
    /// <para>See http://domainlanguage.com/wp-content/uploads/2016/05/DDD_Reference_2015-03.pdf for more details.</para>
    /// </remarks>
    public class ValueObjectBase
    {
    }
}

