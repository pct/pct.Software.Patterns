﻿//
//  Copyright 2014  Michael T. Muessemeyer
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;
using System.Collections.Generic;
using System.Reflection;

namespace pct.Software.Patterns.DDD
{
    /// <summary>
    /// Represents an entity in the domain whose state is impacted by other domain objects that contribute to the 
    /// "wholeness" of this entity.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Cluster the entities and value objects into aggregates and define boundaries around each. Choose 
    /// one entity to be the root of each aggregate, and allow external objects to hold references to the 
    /// root only (references to internal members passed out for use within a single operation only). Define 
    /// properties and invariants for the aggregate as a whole and give enforcement responsibility to the root or 
    /// some designated framework mechanism.
    /// </para>
    /// <para>See http://domainlanguage.com/wp-content/uploads/2016/05/DDD_Reference_2015-03.pdf for more details.</para>
    /// </remarks>
    public abstract class AggregateBase: IAggregate
    {
        /// <summary>
        /// The identifier for this instance. Accessible by derived types.
        /// </summary>
        protected Guid id;

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public Guid Id { get { return id; } }

        /// <summary>
        /// Private field to hold a reference to the new events that have been applied since the instance was constructed.
        /// </summary>
        List<DomainEventBase> currentEvents = new List<DomainEventBase>();

        /// <summary>
        /// Gets a read only collection of events that have been applied since the instance was constructed.
        /// </summary>
        /// <value>The current events.</value>
        public IEnumerable<DomainEventBase> CurrentEvents { get { return currentEvents.AsReadOnly(); } }

        /// <summary>
        /// Use when instantiating from an aggregate from an event stream so that events that have been recorded 
        /// can be applied to the aggregate.
        /// </summary>
        /// <param name="events">
        /// An enumerable collection of <see cref="DomainEventBase"/> representing an aggregate's event history.
        /// </param>
        public void LoadEvents(IEnumerable<DomainEventBase> events)
        {
            foreach(var anEvent in events)
            {
                ApplyChange(anEvent, false);
            }
        }

        /// <summary>
        /// Raises the specified event for the instance to apply the detaisl of the event to the state of the instance.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Assumes derived types will implement privately scoped Apply methods that will handle specific 
        /// derivations of <see cref="DomainEventBase"/>.
        /// </para>
        /// </remarks>
        /// <param name="anEvent">A domain event.</param>
        protected void Raise(DomainEventBase anEvent)
        {
            ApplyChange(anEvent, true);
        }

        /// <summary>
        /// Applies the specified event to the instance.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Assumes derived types will implement privately scoped Apply methods that will handle specific 
        /// derivations of <see cref="DomainEventBase"/>.
        /// </para>
        /// <para>
        /// Uses <see cref="System.Reflection"/> to dynamically determine the correct private Apply method to call.
        /// </para>
        /// </remarks>
        /// <param name="anEvent">A domain event.</param>
        /// <param name="isNew">If set to <c>true</c>, the event is added to the instance's current events collection.</param>
        void ApplyChange(DomainEventBase anEvent, bool isNew)
        {
            //Use reflection to access private members of the derived class
            //then call the "Apply" method with the specified event type as its signature.
            var args = new DomainEventBase[] { anEvent };
            this.GetType().InvokeMember("Apply", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.InvokeMethod, null, this, args);

            if(isNew)
            {
                currentEvents.Add(anEvent);
            }
        }
    }
}

