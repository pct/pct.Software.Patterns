﻿//
//  Copyright 2014  Michael T. Muessemeyer
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;
using System.Collections.Generic;

namespace pct.Software.Patterns.DDD
{
    /// <summary>
    /// Provides a mechanism for interfacing with an upstream context in order to ge a collection of events that 
    /// have occurred in that context in order to apply those events to the implementing context's published language.
    /// </summary>
    public interface IAdapt
    {
        /// <summary>
        /// Consumes an upstream context's interface in order to get the events that have occurred since 
        /// the specified timestamp.
        /// </summary>
        /// <returns>
        /// An enumerable collection of <see cref="DomainEventBase"/> representing the upstream context's current events.
        /// </returns>
        /// <param name="startingAt">A <see cref="DateTime"/> specifying when the earliest event retrieved from the 
        /// upstream context should have occurred.</param>
        IEnumerable<DomainEventBase> GetCurrentEvents(DateTime startingAt);
    }
}

