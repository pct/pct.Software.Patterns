﻿//
//  Copyright 2014  Michael T. Muessemeyer
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;

namespace pct.Software.Patterns.DDD
{
    /// <summary>
    /// Provides a mechanism for representing a uniquely identifiable object within the domain.
    /// </summary>
    /// <remarks>
    /// <para>
    /// When an object is distinguished by its identity, rather than its attributes, make this primary to its 
    /// definition in the model. Keep the class definition simple and focused on life cycle continuity and identity.
    /// </para>
    /// <para>
    /// Define a means of distinguishing each object regardless of its form or history. Be alert to requirements 
    /// that call for matching objects by attributes.
    /// </para>
    /// <para>See http://domainlanguage.com/wp-content/uploads/2016/05/DDD_Reference_2015-03.pdf for more details.</para>
    /// </remarks>
    public interface IIdentifiable
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        Guid Id { get; }
    }
}

