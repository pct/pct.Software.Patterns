﻿//
//  Copyright 2014  Michael T. Muessemeyer
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;
using pct.Software.Patterns.PubSub;

namespace pct.Software.Patterns.DDD
{
    /// <summary>
    /// Represents a transition that has occurred within the domain.
    /// </summary>
    /// <remarks>
    /// <para>
    /// These are distinct from system events that reflect activity within the software itself, although often a system 
    /// event is associated with a domain event, either as part of a response to the domain event or as a way of 
    /// carrying information about the domain event into the system.
    /// </para>
    /// <para>
    /// A domain event is a full-fledged part of the domain model, a representation of something that happened in the 
    /// domain. Ignore irrelevant domain activity while making explicit the events that the domain experts want to 
    /// track or be notified of, or which are associated with state change in the other model objects.
    /// </para>
    /// <para>See http://domainlanguage.com/wp-content/uploads/2016/05/DDD_Reference_2015-03.pdf for more details.</para>
    /// </remarks>
    public abstract class DomainEventBase: INotify
    {
        /// <summary>
        /// Gets the <see cref="DateTime"/> this event occurred. 
        /// </summary>
        /// <value>The time the event occurred.</value>
        public DateTime Occurred { get; protected set; }

        /// <summary>
        /// An integer representing the version of the <see cref="IAggregate"/>  that raised the event.
        /// </summary>
        public int Version;

        /// <summary>
        /// Delivers the notification to the specified <see cref="ISubscribe"/>.
        /// </summary>
        /// <param name="aSubscriber">A subscriber.</param>
        public void Notify(ISubscribe aSubscriber)
        {
            aSubscriber.Deliver(this);
        }
    }
}
