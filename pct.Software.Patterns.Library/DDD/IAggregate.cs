﻿//
//  Copyright 2014  Michael T. Muessemeyer
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;
using System.Collections.Generic;

namespace pct.Software.Patterns.DDD
{
    /// <summary>
    /// Provides a mechanism for representing an entity in the domain whose state is impacted by other domain objects 
    /// that contribute to the "wholeness" of this entity.
    /// </summary>
    public interface IAggregate: IIdentifiable
    {
        /// <summary>
        /// Gets a collecton of <see cref="DomainEventBase"/> that have been applied since the instance was created 
        /// within a given context. 
        /// </summary>
        /// <value>The current events.</value>
        IEnumerable<DomainEventBase> CurrentEvents { get; }

        /// <summary>
        /// Use when instantiating from an aggregate from an event stream so that events that have been recorded 
        /// can be applied to the aggregate.
        /// </summary>
        /// <param name="eventHistory">
        /// An enumerable collection of <see cref="DomainEventBase"/> representing an aggregate's event history.
        /// </param>
        void LoadEvents(IEnumerable<DomainEventBase> eventHistory);
    }
}

