﻿//
//  Copyright 2014  Michael T. Muessemeyer
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;

namespace pct.Software.Patterns.DDD
{
    /// <summary>
    /// Provides a mechanism for translating events of type <typeparamref name="T"/> from an upstream bounded context 
    /// into the implementing type's context in order to enable consistency within the published language.
    /// </summary>
    /// <typeparam name="T">A type that derives from <see cref="UpstreamEventBase"/> </typeparam>
    public interface ITranslate<T> where T: UpstreamEventBase
    {
        /// <summary>
        /// Translate the specified <see cref="UpstreamEventBase"/> into a corresponding <see cref="DomainEventBase"/>.
        /// </summary>
        /// <param name="anUpstreamEvent">An upstream bounded context's event.</param>
        /// <returns><see cref="DomainEventBase"/> that can be applied to the published language of the implementing context.</returns>
        DomainEventBase Translate(T anUpstreamEvent);
    }
}

